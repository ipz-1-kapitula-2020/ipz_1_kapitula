package com.izp;

import org.json.JSONObject;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.util.*;

public class Client {

    public static void main(String[] args) throws IOException {
        List<Integer> list1 = Arrays.asList(1, 2, 3, 4, 5, 6);
        List<Integer> list2 = Arrays.asList(11, 12, 13, 4, 6);
        Long start = new Date().getTime();
        exec(list1, list2);
        Long end = new Date().getTime();
        System.out.println("exec: " + (end - start));

        Long start1 = new Date().getTime();
        exec2(list1, list2);
        Long end2 = new Date().getTime();
        System.out.println("exec: " + (end2 - start1));

    }

    public static void exec(List<Integer> list1, List<Integer> list2) {
        List<Integer> copyList = new ArrayList<>(list1);
        copyList.removeIf(list2::contains);
    }

    public static void exec2(List<Integer> list1, List<Integer> list2) {
        List<Integer> copyList = new ArrayList<>(list1);
        for (int i = 0; i < list1.size(); i++) {
            copyList.remove(list1.get(i));
        }
    }

    private static void registrationExample() throws IOException {
        Socket socket = new Socket("localhost", 8081);
        OutputStreamWriter writer = new OutputStreamWriter(socket.getOutputStream(), "UTF-8");
        BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "UTF-8"));

        JSONObject jsonRequest = new JSONObject();
        jsonRequest.put("tcp-method", "USER_REGISTRATION");
        jsonRequest.put("name", "Name");
        jsonRequest.put("surname", "Surname");
        jsonRequest.put("login", "login");
        jsonRequest.put("password", "password");
        jsonRequest.put("identificationCode", "identificationCode");

        writer.write(jsonRequest.toString() + "\n");
        writer.flush();


        String response = reader.readLine();
        JSONObject jsonResponse = new JSONObject(response);

        System.out.println(jsonResponse.getString("status"));
        System.out.println(jsonResponse.getString("message"));
        socket.close();
    }

    private static void authorizationExample() throws IOException {
        Socket socket = new Socket("localhost", 8081);

        OutputStreamWriter writer = new OutputStreamWriter(socket.getOutputStream(), "UTF-8");
        BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "UTF-8"));

        JSONObject jsonRequest = new JSONObject();
        jsonRequest.put("tcp-method", "USER_AUTHORIZATION");
        jsonRequest.put("login", "login");
        jsonRequest.put("password", "password");

        writer.write(jsonRequest.toString() + "\n");
        writer.flush();


        String response = reader.readLine();
        JSONObject jsonResponse = new JSONObject(response);

        System.out.println(jsonResponse.getString("status"));
        System.out.println(jsonResponse.getString("message"));
        socket.close();
    }

    private static void registrationStudentExample() throws IOException {
        Socket socket = new Socket("localhost", 8081);

        OutputStreamWriter writer = new OutputStreamWriter(socket.getOutputStream(), "UTF-8");
        BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "UTF-8"));

        JSONObject jsonRequest = new JSONObject();
        jsonRequest.put("tcp-method", "STUDENT_REGISTRATION");
        jsonRequest.put("name", "Name");
        jsonRequest.put("surname", "Surname");
        jsonRequest.put("dob", "2000-01-01");
        jsonRequest.put("identificationCode", "123456789");
        jsonRequest.put("universityName", "universityName");
        jsonRequest.put("specialityName", "specialityName");
        jsonRequest.put("courseNumber", "1");


        writer.write(jsonRequest.toString() + "\n");
        writer.flush();


        String response = reader.readLine();
        JSONObject jsonResponse = new JSONObject(response);

        System.out.println(jsonResponse.getString("status"));
        System.out.println(jsonResponse.getString("message"));
        socket.close();
    }

    private static void updateStudentExample() throws IOException {
        Socket socket = new Socket("localhost", 8081);

        OutputStreamWriter writer = new OutputStreamWriter(socket.getOutputStream(), "UTF-8");
        BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "UTF-8"));

        JSONObject jsonRequest = new JSONObject();
        jsonRequest.put("tcp-method", "STUDENT_UPDATE");
        jsonRequest.put("identificationCode", "123456789");
        jsonRequest.put("universityName", "LPNU");
        jsonRequest.put("specialityName", "KI");
        jsonRequest.put("courseNumber", "3");

        writer.write(jsonRequest.toString() + "\n");
        writer.flush();


        String response = reader.readLine();
        JSONObject jsonResponse = new JSONObject(response);

        System.out.println(jsonResponse.getString("status"));
        System.out.println(jsonResponse.getString("message"));
        socket.close();
    }

    private static void deleteStudentExample() throws IOException {
        Socket socket = new Socket("localhost", 8081);

        OutputStreamWriter writer = new OutputStreamWriter(socket.getOutputStream(), "UTF-8");
        BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "UTF-8"));

        JSONObject jsonRequest = new JSONObject();
        jsonRequest.put("tcp-method", "STUDENT_DELETE");
        jsonRequest.put("identificationCode", "123456789");

        writer.write(jsonRequest.toString() + "\n");
        writer.flush();


        String response = reader.readLine();
        JSONObject jsonResponse = new JSONObject(response);

        System.out.println(jsonResponse.getString("status"));
        System.out.println(jsonResponse.getString("message"));
        socket.close();
    }

    private static void settlementExample() throws IOException {
        Socket socket = new Socket("localhost", 8081);

        OutputStreamWriter writer = new OutputStreamWriter(socket.getOutputStream(), "UTF-8");
        BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "UTF-8"));

        JSONObject jsonRequest = new JSONObject();
        jsonRequest.put("tcp-method", "SETTLEMENT");
        jsonRequest.put("chummeryNumber", "1");
        jsonRequest.put("roomNumber", "1");
        jsonRequest.put("studentIdentificationCode", "123456789");

        writer.write(jsonRequest.toString() + "\n");
        writer.flush();


        String response = reader.readLine();
        JSONObject jsonResponse = new JSONObject(response);

        System.out.println(jsonResponse.getString("status"));
        System.out.println(jsonResponse.getString("message"));
        socket.close();
    }

    private static void evictionExample() throws IOException {
        Socket socket = new Socket("localhost", 8081);

        OutputStreamWriter writer = new OutputStreamWriter(socket.getOutputStream(), "UTF-8");
        BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "UTF-8"));

        JSONObject jsonRequest = new JSONObject();
        jsonRequest.put("tcp-method", "EVICTION");
        jsonRequest.put("studentIdentificationCode", "123456789");

        writer.write(jsonRequest.toString() + "\n");
        writer.flush();


        String response = reader.readLine();
        JSONObject jsonResponse = new JSONObject(response);

        System.out.println(jsonResponse.getString("status"));
        System.out.println(jsonResponse.getString("message"));
        socket.close();
    }

}
