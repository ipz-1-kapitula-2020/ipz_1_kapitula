package com.izp.server.controller.mapper;

import com.izp.entity.User;
import org.json.JSONObject;

public class UserMapper {
    public static User jsonToUser(JSONObject json) {
        User user = new User();
        user.setName(json.getString("name"));
        user.setSurname(json.getString("surname"));
        user.setLogin(json.getString("login"));
        user.setPassword(json.getString("password"));
        user.setIdentificationCode(json.getString("identificationCode"));
        return user;
    }
}
