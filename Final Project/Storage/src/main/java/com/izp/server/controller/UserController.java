package com.izp.server.controller;

import com.izp.server.ResponseStatus;
import com.izp.server.controller.mapper.UserMapper;
import com.izp.service.UserService;
import com.izp.service.impl.UserServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;

@Slf4j
public class UserController {
    private UserService service;

    public UserController() {
        service = new UserServiceImpl();
    }

    public String registration(JSONObject json) {
        JSONObject jsonResponse = new JSONObject();
        try {

            if (service.registration(UserMapper.jsonToUser(json))) {
                jsonResponse.put("status", ResponseStatus.CREATED);
                jsonResponse.put("message", "registration was successful");

                log.info("user with ic {} registered",
                        json.getString("identificationCode"));

                return jsonResponse.toString() + Constant.RESPONSE_DELIMITER;
            }
            jsonResponse.put("status", ResponseStatus.ERROR);
            jsonResponse.put("message", "registration fail");
            return jsonResponse.toString() + Constant.RESPONSE_DELIMITER;
        } catch (RuntimeException e) {
            jsonResponse.put("status", ResponseStatus.ERROR);
            jsonResponse.put("message", "Some trouble on server: " + e.getMessage());
            return jsonResponse.toString() + Constant.RESPONSE_DELIMITER;
        }
    }

    public String authorization(JSONObject json) {
        JSONObject jsonResponse = new JSONObject();
        try {
            String login = json.getString("login");
            String password = json.getString("password");

            if (service.authorization(login, password)) {
                jsonResponse.put("status", ResponseStatus.AUTHORIZED);
                jsonResponse.put("message", "authorization was successful");

                log.info("user with login {} authorized",
                        json.getString("login"));

                return jsonResponse.toString() + Constant.RESPONSE_DELIMITER;
            }

            jsonResponse.put("status", ResponseStatus.ERROR);
            jsonResponse.put("message", "authorization fail");
            return jsonResponse.toString() + Constant.RESPONSE_DELIMITER;

        } catch (RuntimeException e) {
            log.error(e.getMessage());

            jsonResponse.put("status", ResponseStatus.ERROR);
            jsonResponse.put("message", "Some trouble on server: " + e.getMessage());
            return jsonResponse.toString() + Constant.RESPONSE_DELIMITER;
        }
    }
}
