package com.izp.server;

import com.izp.server.controller.ProductController;
import com.izp.server.controller.UserController;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;

public class Server {

    public static final String CHARSET_NAME = "UTF-8";

    public static void run(Socket socket) {
        Thread thread = new Thread(() -> {
            try {
                OutputStreamWriter writer =
                        new OutputStreamWriter(socket.getOutputStream(), CHARSET_NAME);

                BufferedReader reader =
                        new BufferedReader(new InputStreamReader(socket.getInputStream(), CHARSET_NAME));

                String request = reader.readLine();
                JSONObject jsonRequest = new JSONObject(request);

                String response = execute(jsonRequest);

                writer.write(response);
                writer.flush();

            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                closeSocket(socket);
            }
        });
        thread.start();
    }

    private static String execute(JSONObject jsonRequest) {
        TcpMethod tcpMethod = TcpMethod.NONE;
        String response = getDefaultResponse();

        if(jsonRequest.has("tcp-method")) {
            tcpMethod = TcpMethod.valueOf(jsonRequest.getString("tcp-method"));
        }

        UserController userController = new UserController();
        ProductController productController = new ProductController();


        switch (tcpMethod) {
            case NONE:
                break;
            case USER_REGISTRATION:
                response = userController.registration(jsonRequest);
                break;
            case USER_AUTHORIZATION:
                response = userController.authorization(jsonRequest);
                break;
            case SUPPLY:
                response = productController.supply(jsonRequest);
                break;
            case DISSEMINATION:
                response = productController.dissemination(jsonRequest);
                break;

        }
        return response;
    }

    private static String getDefaultResponse() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("status", ResponseStatus.TCP_METHOD_NOT_FOUND);
        jsonObject.put("message", "tcp-method is absent");

        return jsonObject.toString();
    }

    private static void closeSocket(Socket socket) {
        try {
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
