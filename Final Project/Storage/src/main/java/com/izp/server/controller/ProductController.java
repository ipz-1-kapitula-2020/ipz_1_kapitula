package com.izp.server.controller;

import com.izp.entity.Product;
import com.izp.server.ResponseStatus;
import com.izp.server.controller.mapper.ProductMapper;
import com.izp.server.controller.mapper.UserMapper;
import com.izp.service.ProductService;
import com.izp.service.impl.ProductServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;

@Slf4j
public class ProductController {
    private final ProductService service = new ProductServiceImpl();

    public String supply(JSONObject json) {
        JSONObject jsonResponse = new JSONObject();
        try {

            if (service.saveProduct(ProductMapper.jsonToProduct(json))) {
                jsonResponse.put("status", ResponseStatus.CREATED);
                jsonResponse.put("message", "save product was successful");

                log.info("product with name {} registered",
                        json.getString("name"));

                return jsonResponse.toString() + Constant.RESPONSE_DELIMITER;
            }
            jsonResponse.put("status", ResponseStatus.ERROR);
            jsonResponse.put("message", "save fail");
            return jsonResponse.toString() + Constant.RESPONSE_DELIMITER;
        } catch (RuntimeException e) {
            jsonResponse.put("status", ResponseStatus.ERROR);
            jsonResponse.put("message", "Some trouble on server: " + e.getMessage());
            return jsonResponse.toString() + Constant.RESPONSE_DELIMITER;
        }
    }

    public String  dissemination(JSONObject json) {
        JSONObject jsonResponse = new JSONObject();
        try {

            if (service.changeStatus(ProductMapper.jsonToProduct(json))) {
                jsonResponse.put("status", ResponseStatus.CREATED);
                jsonResponse.put("message", "update product was successful");

                log.info("product, named {}, updated with status: {}",
                        json.getString("name"),
                        json.getString("order_status")
                        );

                return jsonResponse.toString() + Constant.RESPONSE_DELIMITER;
            }
            jsonResponse.put("status", ResponseStatus.ERROR);
            jsonResponse.put("message", "update fail");
            return jsonResponse.toString() + Constant.RESPONSE_DELIMITER;
        } catch (RuntimeException e) {
            jsonResponse.put("status", ResponseStatus.ERROR);
            jsonResponse.put("message", "Some trouble on server: " + e.getMessage());
            return jsonResponse.toString() + Constant.RESPONSE_DELIMITER;
        }
    }
}
