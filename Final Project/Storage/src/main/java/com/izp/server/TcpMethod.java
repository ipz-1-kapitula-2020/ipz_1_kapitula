package com.izp.server;

public enum TcpMethod {
    NONE,
    USER_REGISTRATION,
    USER_AUTHORIZATION,
    SUPPLY,
    DISSEMINATION
}
