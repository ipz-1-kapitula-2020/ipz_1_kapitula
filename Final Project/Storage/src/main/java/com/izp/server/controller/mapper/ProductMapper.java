package com.izp.server.controller.mapper;

import com.izp.entity.Product;
import com.izp.entity.User;
import org.json.JSONObject;

public class ProductMapper {
    public static Product jsonToProduct(JSONObject json) {
        Product product = new Product();
        product.setCategory(json.getString("category"));
        product.setCustomer(json.getString("customer"));
        product.setName(json.getString("name"));
        product.setModel(json.getString("model"));
        product.setBrand(json.getString("brand"));
        product.setStatus(json.getString("order_status"));

        return product;
    }
}
