package com.izp;

import com.izp.server.Server;


import java.io.IOException;

import java.net.ServerSocket;
import java.net.Socket;

public class App {
    public static void main(String[] args) throws IOException {
        ServerSocket serverSocket = new ServerSocket(8081);

        try {
            while (true) {
                Socket socket = serverSocket.accept();
                Server.run(socket);
            }
        } finally {
            serverSocket.close();
        }
    }

}

