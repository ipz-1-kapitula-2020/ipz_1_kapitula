package com.izp.util;

import com.izp.dao.builder.InstanceBuilder;

import java.sql.Date;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import lombok.extern.slf4j.Slf4j;


@Slf4j
public class CrudUtils {


    /**
     * Method gets object of type TEntity
     * from database.
     */
    public static <TEntity> Optional<TEntity> getEntity(
            Connection connection, InstanceBuilder<TEntity> builder, String sqlQuery, Object... args) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery)) {
            setArgsToStatement(preparedStatement, args);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    return Optional.of(builder.createInstance(resultSet));
                }
            }
            return Optional.empty();
        } catch (SQLException e) {
            log.error("SQLException: {}", e.getMessage());

            throw new RuntimeException(e);
        }
    }

    /**
     * Method gets list objects of type TEntity
     * from database.
     * @return list of TEntity object
     */
    public static <TEntity> List<TEntity> getEntityList(
            Connection connection, InstanceBuilder<TEntity> builder, String sqlQuery, Object... args) {
        List<TEntity> entityList = new ArrayList<>();
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery)) {
            setArgsToStatement(preparedStatement, args);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    entityList.add(builder.createInstance(resultSet));
                }
                return entityList;
            }
        } catch (SQLException e) {
            log.error("SQLException: {}", e.getMessage());

            throw new RuntimeException(e);
        }
    }

    /**
     * Method inserts/updates/deletes objects
     * in database.
     * @return number of changed objects in database
     */
    public static int update(Connection connection, String sqlQuery, Object... args) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery)) {
            setArgsToStatement(preparedStatement, args);
            return preparedStatement.executeUpdate();
        } catch (SQLException e) {
            log.error("SQLException: {}", e.getMessage());

            throw new RuntimeException(e);
        }
    }

    /**
     * Method sets arguments in prepared statement.
     */
    private static void setArgsToStatement(PreparedStatement preparedStatement, Object... args) {
        try {
            for (int i = 0; i < args.length; i++) {
                if (args[i] == null) {
                    preparedStatement.setNull(i + 1, Types.NULL);
                } else if (args[i].getClass().equals(String.class)) {
                    preparedStatement.setString(i + 1, (String) args[i]);
                } else if (args[i].getClass().equals(Long.class)) {
                    preparedStatement.setLong(i + 1, (Long) args[i]);
                } else if (args[i].getClass().equals(Date.class)) {
                    preparedStatement.setDate(i + 1, (Date) args[i]);
                }
            }
        } catch (SQLException e) {
            log.error("SQLException: {}", e.getMessage());

            throw new RuntimeException(e);
        }
    }
}