package com.izp.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@EqualsAndHashCode(callSuper = false)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Product extends Entity {
    private String category;
    private String brand;
    private String name;
    private String model;
    private String status;
    private String customer;

    public enum productSqlQuery {
        INSERT(SqlQuery.INSERT,
                "insert into product (category, brand, name, model, status, customer) " +
                        "values (?, ?, ?, ?, ?, ?);"),
        UPDATE(SqlQuery.UPDATE_BY_FIELD,
                "UPDATE product SET status = ? " +
                        "WHERE customer = ? AND category = ? AND brand = ? AND name = ? AND model = ?;");


        private SqlQuery sqlQuery;
        private String query;

        productSqlQuery(SqlQuery sqlQuery, String query) {
            this.sqlQuery = sqlQuery;
            this.query = query;
        }

        public SqlQuery getSqlQuery() {
            return sqlQuery;
        }

        @Override
        public String toString() {
            return query;
        }
    }

}
