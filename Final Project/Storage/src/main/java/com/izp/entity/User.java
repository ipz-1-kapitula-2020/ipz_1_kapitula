package com.izp.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.sql.Date;

@EqualsAndHashCode(callSuper = false)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class User extends Entity{
    private String name;
    private String surname;
    private String login;
    private String password;
    private String identificationCode;

    public enum userSqlQuery {
        GET_BY_FIELD(SqlQuery.GET_BY_FIELD, "SELECT * FROM user WHERE login = ? AND password = ?;"),
        INSERT(SqlQuery.INSERT,
                "INSERT INTO user (name, surname, login, password, identification_code) VALUES (?, ?, ?, ?, ?);");


        private SqlQuery sqlQuery;
        private String query;

        userSqlQuery(SqlQuery sqlQuery, String query) {
            this.sqlQuery = sqlQuery;
            this.query = query;
        }

        public SqlQuery getSqlQuery() {
            return sqlQuery;
        }

        @Override
        public String toString() {
            return query;
        }
    }
}
