package com.izp.dao.builder;

import com.izp.entity.User;
import lombok.extern.slf4j.Slf4j;

import java.sql.ResultSet;
import java.sql.SQLException;
@Slf4j
public class UserBuilder implements InstanceBuilder<User> {
    public User createInstance(ResultSet resultSet) {
        User user = new User();
        try {
            user.setId(resultSet.getLong("user_id"));
            user.setName(resultSet.getString("name"));
            user.setSurname(resultSet.getString("surname"));
            user.setLogin(resultSet.getString("login"));
            user.setPassword(resultSet.getString("password"));
            log.info("new instance created: {}", user);
            return user;
        } catch (SQLException e) {
            log.error("failed to create instance of `User`");
            throw new RuntimeException();
        }
    }
}
