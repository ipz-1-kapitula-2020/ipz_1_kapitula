package com.izp.dao;

import com.izp.dao.crud.DaoCrudA;
import com.izp.entity.Product;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ProductDao extends DaoCrudA<Product> {
    @Override
    protected Object[] getFields(Product entity) {
        final int NUMBER_OF_FIELDS_IN_ENTITY = 7;

        Object[] fields = new Object[NUMBER_OF_FIELDS_IN_ENTITY];
        fields[0] = entity.getId();
        fields[1] = entity.getCategory();
        fields[2] = entity.getBrand();
        fields[3] = entity.getName();
        fields[4] = entity.getModel();
        fields[5] = entity.getStatus();
        fields[6] = entity.getCustomer();

        log.info("get fields of entity: {}", entity);

        return fields;
    }

    @Override
    protected void init() {
        for (Product.productSqlQuery productSqlQuery : Product.productSqlQuery.values()) {
            sqlQueries.put(productSqlQuery.getSqlQuery(), productSqlQuery);
        }
        log.info("product dao init");
    }
}
