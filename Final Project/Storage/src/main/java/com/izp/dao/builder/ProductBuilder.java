package com.izp.dao.builder;

import com.izp.entity.Product;
import com.izp.entity.User;
import lombok.extern.slf4j.Slf4j;

import java.sql.ResultSet;
import java.sql.SQLException;
@Slf4j
public class ProductBuilder implements InstanceBuilder<Product> {
    @Override
    public Product createInstance(ResultSet resultSet) {
        Product product = new Product();
        try {
            product.setId(resultSet.getLong("product_id"));
            product.setBrand(resultSet.getString("brand"));
            product.setCategory(resultSet.getString("category"));
            product.setCustomer(resultSet.getString("customer"));
            product.setModel(resultSet.getString("model"));
            product.setName(resultSet.getString("name"));
            product.setStatus(resultSet.getString("status"));
            log.info("new instance created: {}", product);
            return product;
        } catch (SQLException e) {
            log.error("failed to create instance of `Product` from ResultSet");
            throw new RuntimeException("failed to create instance of `Product` from ResultSet");
        }
    }
}
