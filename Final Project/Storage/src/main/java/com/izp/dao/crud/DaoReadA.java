package com.izp.dao.crud;


import com.izp.dao.builder.InstanceBuilder;
import com.izp.db.DBConnection;
import com.izp.entity.SqlQuery;
import com.izp.util.CrudUtils;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
@Slf4j
public abstract class DaoReadA<TEntity> implements DaoReadI<TEntity> {

    /**
     * Map stores sql queries.
     */
    protected final Map<Enum<?>, Enum<?>> sqlQueries;

    /**
     * Default constructor
     */
    protected DaoReadA() {
        sqlQueries = new HashMap<>();
        init();
    }

    /**
     * Method gets object type of TEntity from database
     * by id.
     *
     * @param builder
     * @param id
     * @return object type of TEntity
     */
    @Override
    public TEntity getById(InstanceBuilder<TEntity> builder, Long id) {
        try (Connection connection = DBConnection.getConnection()) {
            return CrudUtils.getEntity(connection, builder, sqlQueries.get(SqlQuery.GET_BY_ID).toString(), id).get();
        } catch (SQLException e) {
            log.error("SQLException: {}", e.getMessage());

            throw new RuntimeException(e.getMessage());
        }
    }

    /**
     * Method gets object type of TEntity from database
     * by fields.
     *
     * @param builder
     * @param fields
     * @return list objects type of TEntity
     */
    @Override
    public List<TEntity> getByFields(InstanceBuilder<TEntity> builder, Object... fields) {
        try (Connection connection = DBConnection.getConnection()) {
            return CrudUtils.getEntityList(
                    connection, builder, sqlQueries.get(SqlQuery.GET_BY_FIELD).toString(), fields);
        } catch (SQLException e) {
            log.error("SQLException: {}", e.getMessage());

            throw new RuntimeException(e.getMessage());
        }
    }

    /**
     * Method gets list objects type of TEntity from database
     * by parameters.
     *
     * @param builder
     * @param params
     * @return list objects type of TEntity
     */
    @Override
    public List<TEntity> getAll(InstanceBuilder<TEntity> builder, Object... params) {
        try (Connection connection = DBConnection.getConnection()) {
            return CrudUtils.getEntityList(
                    connection, builder, sqlQueries.get(SqlQuery.GET_ALL).toString(), params);
        } catch (SQLException e) {
            log.error("SQLException: {}", e.getMessage());

            throw new RuntimeException(e.getMessage());
        }
    }

    /**
     * Method initializes query resources.
     */
    protected abstract void init();
}
