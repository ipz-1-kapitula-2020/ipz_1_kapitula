package com.izp.dao;

import com.izp.dao.crud.DaoCrudA;
import com.izp.entity.User;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class UserDao extends DaoCrudA<User> {
    protected Object[] getFields(User entity) {
        final int NUMBER_OF_FIELDS_IN_ENTITY = 6;

        Object[] fields = new Object[NUMBER_OF_FIELDS_IN_ENTITY];
        fields[0] = entity.getId();
        fields[1] = entity.getName();
        fields[2] = entity.getSurname();
        fields[3] = entity.getLogin();
        fields[4] = entity.getPassword();
        fields[5] = entity.getIdentificationCode();

        log.info("get fields of entity: {}", entity);

        return fields;
    }

    protected void init() {
        for (User.userSqlQuery userSqlQuery : User.userSqlQuery.values()) {
            sqlQueries.put(userSqlQuery.getSqlQuery(), userSqlQuery);
        }
        log.info("user dao init");
    }
}
