package com.izp.service;

import com.izp.entity.User;

public interface UserService {
    User getByIdentificationCode(String identificationCode);
    Boolean registration(User user);
    Boolean authorization(String login, String password);
}
