package com.izp.service;

import com.izp.dao.ProductDao;
import com.izp.entity.Product;
import com.sun.org.apache.xpath.internal.operations.Bool;

public interface ProductService {
    Boolean saveProduct(Product product);

    Boolean changeStatus(Product product);
}
