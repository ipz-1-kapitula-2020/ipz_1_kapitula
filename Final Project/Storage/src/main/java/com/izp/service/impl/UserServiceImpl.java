package com.izp.service.impl;

import com.izp.dao.UserDao;
import com.izp.dao.builder.UserBuilder;
import com.izp.entity.User;
import com.izp.service.UserService;


public class UserServiceImpl implements UserService {
    private UserDao repo;

    public UserServiceImpl() {
        repo = new UserDao();
    }

    @Override
    public User getByIdentificationCode(String identificationCode) {
        return (User) repo.getByFields(new UserBuilder(), identificationCode);
    }

    @Override
    public Boolean registration(User user) {
        return repo.insert(user);
    }

    @Override
    public Boolean authorization(String login, String password) {

        return !repo.getByFields(new UserBuilder(), login, password).isEmpty();
    }
}
