package com.izp.service.impl;

import com.izp.dao.ProductDao;
import com.izp.entity.Product;
import com.izp.service.ProductService;

public class ProductServiceImpl implements ProductService {
    private final ProductDao productDao = new ProductDao();

    @Override
    public Boolean saveProduct(Product product) {
        return productDao.insert(product);
    }

    @Override
    public Boolean changeStatus(Product product) {
        return productDao.updateByFields(
                product.getStatus(),
                product.getCustomer(),
                product.getCategory(),
                product.getBrand(),
                product.getName(),
                product.getModel()
        );
    }
}
