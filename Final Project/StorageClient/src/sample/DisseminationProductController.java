package sample;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.json.JSONObject;
import sample.controller.util.*;

public class DisseminationProductController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button backButton;

    @FXML
    private Button disseminationButton;

    @FXML
    private TextField modelField;

    @FXML
    private TextField customerField;

    @FXML
    private ChoiceBox<String> categoryChoiceBox;

    @FXML
    private ChoiceBox<String> brandChoiceBox;

    @FXML
    private TextField nameField;

    @FXML
    private ChoiceBox<String> statusChoiceBox;

    private ObservableList<String> categoryList = FXCollections.observableArrayList("Smartphone", "Laptop");
    private ObservableList<String> brandList = FXCollections.observableArrayList("Samsung", "Apple");
    private ObservableList<String> statusList = FXCollections.observableArrayList(
            OrderStatus.IN_STORAGE.toString(),
            OrderStatus.PACKED.toString(),
            OrderStatus.IN_THE_WAY.toString(),
            OrderStatus.DELIVERED.toString());

    @FXML
    void initialize() {
        categoryChoiceBox.setValue("Smartphone");
        categoryChoiceBox.setItems(categoryList);
        brandChoiceBox.setValue("Samsung");
        brandChoiceBox.setItems(brandList);
        statusChoiceBox.setValue(OrderStatus.IN_STORAGE.toString());
        statusChoiceBox.setItems(statusList);

        disseminationButton.setOnAction(event -> dissemination());

        backButton.setOnAction(this::changeWindow);
    }

    private void dissemination() {
        try {
            JSONObject jsonResponse = executeDissemination();
            if (jsonResponse.has("parametersInvalid")) {
                AlertWindow.showInvalidParamWindow(jsonResponse);
            } else {
                AlertWindow.showResponseWindow(jsonResponse);
            }
        } catch (IOException ex) {
            AlertWindow.showErrorWindow();
        }
    }

    private JSONObject executeDissemination() throws IOException {
        JSONObject jsonResponse = new JSONObject();
        if (CheckParameters.checkName(nameField.getText()) &&
                CheckParameters.checkCustomer(customerField.getText())
        ) {

            JSONObject jsonRequest = new JSONObject();
            jsonRequest.put("tcp-method", TcpMethod.DISSEMINATION);
            jsonRequest.put("category", categoryChoiceBox.getValue());
            jsonRequest.put("customer", customerField.getText());
            jsonRequest.put("name", nameField.getText());
            jsonRequest.put("model", modelField.getText());
            jsonRequest.put("brand", brandChoiceBox.getValue());
            jsonRequest.put("order_status", statusChoiceBox.getValue());

            Socket socket = SocketUtil.getSocket();
            OutputStreamWriter writer = SocketUtil.getOutputStream(socket);
            BufferedReader reader = SocketUtil.getBufferedReader(socket);

            writer.write(jsonRequest.toString() + "\n");
            writer.flush();

            String response = reader.readLine();

            socket.close();

            return new JSONObject(response);
        }

        jsonResponse.put("parametersInvalid", "invalid input data");

        return jsonResponse;
    }

    private void changeWindow(ActionEvent event) {
        try {
            showMenuWindow(event);
        } catch (IOException e) {
            AlertWindow.showErrorWindow();
        }
    }

    private void showMenuWindow(ActionEvent event) throws IOException {
        Parent parent = FXMLLoader.load(getClass().getResource("menu.fxml"));
        Scene scene = new Scene(parent);
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
        window.setScene(scene);
        window.show();
    }
}
