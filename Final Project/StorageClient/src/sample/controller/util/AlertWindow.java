package sample.controller.util;

import javafx.scene.control.Alert;
import org.json.JSONObject;

public class AlertWindow {
    public static void showInvalidParamWindow(JSONObject jsonResponse) {
        Alert a = new Alert(Alert.AlertType.WARNING);
        a.setContentText(jsonResponse.getString("parametersInvalid"));
        a.show();
    }

    public static void showResponseWindow(JSONObject jsonResponse) {
        Alert a = new Alert(Alert.AlertType.INFORMATION);
        a.setHeaderText(jsonResponse.getString("status"));
        a.setContentText(jsonResponse.getString("message"));
        a.show();
    }
    public static void showErrorWindow() {
        Alert a = new Alert(Alert.AlertType.ERROR);
        a.setHeaderText("Some error");
        a.show();

    }
}
