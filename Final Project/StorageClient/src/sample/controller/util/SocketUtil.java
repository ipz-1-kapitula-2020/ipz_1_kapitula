package sample.controller.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;

public class SocketUtil {

    private static final String UTF_8 = "UTF-8";

    public static Socket getSocket() throws IOException {
        return new Socket("localhost", 8081);
    }

    public static void closeSocket(Socket socket) throws IOException {
        socket.close();
    }

    public static OutputStreamWriter getOutputStream(Socket socket) throws IOException {
        return new OutputStreamWriter(socket.getOutputStream(), UTF_8);
    }

    public static BufferedReader getBufferedReader(Socket socket) throws IOException {
        return new BufferedReader(new InputStreamReader(socket.getInputStream(), UTF_8));
    }
}
