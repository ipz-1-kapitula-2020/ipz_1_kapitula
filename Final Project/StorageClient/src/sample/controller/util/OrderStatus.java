package sample.controller.util;

public enum OrderStatus {
    IN_STORAGE("in_storage"),
    PACKED("packed"),
    IN_THE_WAY("in_the_way"),
    DELIVERED("delivered");


    private String status;

    OrderStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return status;
    }
}
