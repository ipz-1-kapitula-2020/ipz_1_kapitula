package sample.controller.util;

import com.sun.org.apache.xpath.internal.operations.Bool;

import java.sql.Date;
import java.util.regex.Pattern;

public class CheckParameters {
//    public static Boolean check(String){}

    public static boolean checkLogin(String login) {
        Pattern pattern = Pattern.compile("^(.+)@(.+)\\.(\\w{2,5})$");
        return pattern.matcher(login).find();
    }

    public static Boolean checkName(String name) {
        Pattern pattern = Pattern.compile("^[A-Z](.+)$");
        return pattern.matcher(name).find();
    }

    public static Boolean checkCustomer(String customer) {
        Pattern pattern = Pattern.compile("^[A-Z]([a-z]+) [A-Z]([a-z]+)$");
        return pattern.matcher(customer).find();
    }

    public static boolean checkPassword(String password) {
        Pattern pattern = Pattern.compile("^(.+)$");
        return pattern.matcher(password).find();
    }

    public static boolean checkIdentificationCode(String identificationCode) {
        Pattern pattern = Pattern.compile("^(\\d{9})$");
        return pattern.matcher(identificationCode).find();
    }

    public static boolean checkDob(String dob) {
        Pattern pattern = Pattern.compile("^(\\d{4})-(\\d{2})-(\\d{2})$");
        return pattern.matcher(dob).find();
    }

    public static boolean checkNameSurname(String name, String surname) {
        Pattern pattern = Pattern.compile("^[A-Z]([a-z]+)$");
        return pattern.matcher(name).find() && pattern.matcher(surname).find();
    }
}
