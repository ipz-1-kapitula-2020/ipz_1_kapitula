package sample.controller.util;

public enum ResponseStatus {
    OK,
    CREATED,
    UPDATED,
    DELETED,
    AUTHORIZED,
    ERROR,
    TCP_METHOD_NOT_FOUND
}
