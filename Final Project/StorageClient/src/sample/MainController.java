package sample;

import javafx.event.ActionEvent;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import org.json.JSONObject;
import sample.controller.util.AlertWindow;

public class MainController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button loginButton;

    @FXML
    private Button registrationButton;

    @FXML
    void initialize() {
        registrationButton.setOnAction(event -> {
            try {
                showWindow(event, "registration.fxml");
            } catch (IOException e) {
                AlertWindow.showErrorWindow();
            }
        });

        loginButton.setOnAction(event -> {
            try {
                showWindow(event, "login.fxml");
            } catch (IOException e) {
                AlertWindow.showErrorWindow();
            }
        });

    }

    private void showWindow(ActionEvent event, String windowName) throws IOException {
        Parent parent = FXMLLoader.load(getClass().getResource(windowName));
        Scene scene = new Scene(parent);
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        window.setScene(scene);
        window.show();
    }
}
