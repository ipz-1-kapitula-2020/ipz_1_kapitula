package sample;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.json.JSONObject;
import sample.controller.util.*;

public class LoginController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button backButton;

    @FXML
    private Button loginButton;

    @FXML
    private TextField loginField;

    @FXML
    private PasswordField passwordField;


    @FXML
    void initialize() {
        backButton.setOnAction(this::changeToMainWindow);

        loginButton.setOnAction(this::login);
    }


    private void login(ActionEvent event) {
        try {
            JSONObject jsonResponse = executeLogin();
            if (jsonResponse.has("parametersInvalid")) {
                AlertWindow.showInvalidParamWindow(jsonResponse);
            }
            if (jsonResponse.getString("status").equals("ERROR")) {
                AlertWindow.showResponseWindow(jsonResponse);
            }
            if (jsonResponse.get("status").equals("AUTHORIZED")) {
                changeToMenuWindow(event);
            }

        } catch (IOException ex) {
            AlertWindow.showErrorWindow();
        }
    }

    private JSONObject executeLogin() throws IOException {
        JSONObject jsonResponse = new JSONObject();
        if (
                CheckParameters.checkLogin(loginField.getText()) &&
                        CheckParameters.checkPassword(passwordField.getText())
        ) {

            JSONObject jsonRequest = new JSONObject();
            jsonRequest.put("tcp-method", TcpMethod.USER_AUTHORIZATION);
            jsonRequest.put("login", loginField.getText());
            jsonRequest.put("password", passwordField.getText());


            Socket socket = SocketUtil.getSocket();
            OutputStreamWriter writer = SocketUtil.getOutputStream(socket);
            BufferedReader reader = SocketUtil.getBufferedReader(socket);

            writer.write(jsonRequest.toString() + "\n");
            writer.flush();

            String response = reader.readLine();

            SocketUtil.closeSocket(socket);

            return new JSONObject(response);
        }

        jsonResponse.put("parametersInvalid", "invalid input data");

        return jsonResponse;
    }

    private void changeToMainWindow(ActionEvent event) {
        try {
            showMainWindow(event);
        } catch (IOException e) {
            AlertWindow.showErrorWindow();
        }
    }

    private void changeToMenuWindow(ActionEvent event) {
        try {
            showMenuWindow(event);
        } catch (IOException e) {
            AlertWindow.showErrorWindow();
        }
    }

    private void showMainWindow(ActionEvent event) throws IOException {
        Parent parent = FXMLLoader.load(getClass().getResource("sample.fxml"));
        Scene scene = new Scene(parent);
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
        window.setScene(scene);
        window.show();
    }

    private void showMenuWindow(ActionEvent event) throws IOException {
        Parent parent = FXMLLoader.load(getClass().getResource("menu.fxml"));
        Scene scene = new Scene(parent);
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
        window.setScene(scene);
        window.show();
    }
}
