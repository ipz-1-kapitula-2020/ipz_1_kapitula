package sample;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.json.JSONObject;
import sample.controller.util.*;

public class RegistrationController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button backButton;

    @FXML
    private Button saveButton;

    @FXML
    private TextField nameField;

    @FXML
    private TextField surnameField;

    @FXML
    private TextField dobField;

    @FXML
    private TextField loginField;

    @FXML
    private TextField passwordField;

    @FXML
    private TextField identificationCodeField;

    @FXML
    void initialize() {
        saveButton.setOnAction(e -> {
            registration();
        });

        backButton.setOnAction(this::changeWindow);
    }


    private void registration() {
        try {
            JSONObject jsonResponse = executeRegistration();
            if (jsonResponse.has("parametersInvalid")) {
                AlertWindow.showInvalidParamWindow(jsonResponse);
            } else if (jsonResponse.getString("status").equals(ResponseStatus.ERROR)) {
                throw new IOException();
            } else {
                AlertWindow.showResponseWindow(jsonResponse);
            }
        } catch (IOException ex) {
            AlertWindow.showErrorWindow();
        }
    }

    private JSONObject executeRegistration() throws IOException {
        JSONObject jsonResponse = new JSONObject();
        if (CheckParameters.checkNameSurname(nameField.getText(), surnameField.getText()) &&
                CheckParameters.checkIdentificationCode(identificationCodeField.getText()) &&
                CheckParameters.checkLogin(loginField.getText()) &&
                CheckParameters.checkPassword(passwordField.getText()) &&
                CheckParameters.checkDob(dobField.getText())) {

            JSONObject jsonRequest = new JSONObject();
            jsonRequest.put("tcp-method", TcpMethod.USER_REGISTRATION);
            jsonRequest.put("name", nameField.getText());
            jsonRequest.put("surname", surnameField.getText());
            jsonRequest.put("login", loginField.getText());
            jsonRequest.put("password", passwordField.getText());
            jsonRequest.put("identificationCode", identificationCodeField.getText());


            Socket socket = SocketUtil.getSocket();
            OutputStreamWriter writer = SocketUtil.getOutputStream(socket);
            BufferedReader reader = SocketUtil.getBufferedReader(socket);

            writer.write(jsonRequest.toString() + "\n");
            writer.flush();

            String response = reader.readLine();

            socket.close();

            return new JSONObject(response);
        }

        jsonResponse.put("parametersInvalid", "invalid input data");

        return jsonResponse;
    }

    private void changeWindow(ActionEvent event) {
        try {
            showMainWindow(event);
        } catch (IOException e) {
            AlertWindow.showErrorWindow();
        }
    }

    private void showMainWindow(ActionEvent event) throws IOException {
        Parent parent = FXMLLoader.load(getClass().getResource("sample.fxml"));
        Scene scene = new Scene(parent);
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
        window.setScene(scene);
        window.show();
    }
}
