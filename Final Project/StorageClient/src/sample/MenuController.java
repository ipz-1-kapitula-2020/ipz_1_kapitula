package sample;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import sample.controller.util.AlertWindow;

public class MenuController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button supplyButton;

    @FXML
    private Button disseminationButton;


    @FXML
    void initialize() {
        supplyButton.setOnAction(this::changeToSupplyWindow);
        disseminationButton.setOnAction(this::changeToDisseminationWindow);
    }

    private void changeToSupplyWindow(ActionEvent event) {
        try {
            showSupplyWindow(event);
        } catch (IOException e) {
            AlertWindow.showErrorWindow();
        }
    }

    private void changeToDisseminationWindow(ActionEvent event) {
        try {
            showDisseminationWindow(event);
        } catch (IOException e) {
            AlertWindow.showErrorWindow();
        }
    }

    private void showSupplyWindow(ActionEvent event) throws IOException {
        Parent parent = FXMLLoader.load(getClass().getResource("supply-product.fxml"));
        Scene scene = new Scene(parent);
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
        window.setScene(scene);
        window.show();
    }

    private void showDisseminationWindow(ActionEvent event) throws IOException {
        Parent parent = FXMLLoader.load(getClass().getResource("dissemination-product.fxml"));
        Scene scene = new Scene(parent);
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
        window.setScene(scene);
        window.show();
    }


}
