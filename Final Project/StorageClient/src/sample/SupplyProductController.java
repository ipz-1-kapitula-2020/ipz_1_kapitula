package sample;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.json.JSONObject;
import sample.controller.util.*;

public class SupplyProductController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button backButton;

    @FXML
    private Button supplyButton;

    @FXML
    private TextField modelField;

    @FXML
    private TextField customerField;

    @FXML
    private ChoiceBox<String> categoryChoiceBox;

    @FXML
    private ChoiceBox<String> brandChoiceBox;

    @FXML
    private TextField nameField;

    private ObservableList<String> categoryList = FXCollections.observableArrayList("Smartphone", "Laptop");
    private ObservableList<String> brandList = FXCollections.observableArrayList("Samsung", "Apple");

    @FXML
    void initialize() {
        categoryChoiceBox.setValue("Smartphone");
        categoryChoiceBox.setItems(categoryList);
        brandChoiceBox.setValue("Samsung");
        brandChoiceBox.setItems(brandList);

        supplyButton.setOnAction(event -> supply());

        backButton.setOnAction(this::changeWindow);


    }

    private void supply() {
        try {
            JSONObject jsonResponse = executeSupply();
            if (jsonResponse.has("parametersInvalid")) {
                AlertWindow.showInvalidParamWindow(jsonResponse);
            } else {
                AlertWindow.showResponseWindow(jsonResponse);
            }
        } catch (IOException ex) {
            AlertWindow.showErrorWindow();
        }
    }

    private JSONObject executeSupply() throws IOException {
        JSONObject jsonResponse = new JSONObject();
        if (CheckParameters.checkName(nameField.getText()) &&
                CheckParameters.checkCustomer(customerField.getText())
        ) {

            JSONObject jsonRequest = new JSONObject();
            jsonRequest.put("tcp-method", TcpMethod.SUPPLY);
            jsonRequest.put("category", categoryChoiceBox.getValue());
            jsonRequest.put("customer", customerField.getText());
            jsonRequest.put("name", nameField.getText());
            jsonRequest.put("model", modelField.getText());
            jsonRequest.put("brand", brandChoiceBox.getValue());
            jsonRequest.put("order_status", OrderStatus.IN_STORAGE.toString());

            Socket socket = SocketUtil.getSocket();
            OutputStreamWriter writer = SocketUtil.getOutputStream(socket);
            BufferedReader reader = SocketUtil.getBufferedReader(socket);

            writer.write(jsonRequest.toString() + "\n");
            writer.flush();

            String response = reader.readLine();

            socket.close();

            return new JSONObject(response);
        }

        jsonResponse.put("parametersInvalid", "invalid input data");

        return jsonResponse;
    }

    private void changeWindow(ActionEvent event) {
        try {
            showMenuWindow(event);
        } catch (IOException e) {
            AlertWindow.showErrorWindow();
        }
    }

    private void showMenuWindow(ActionEvent event) throws IOException {
        Parent parent = FXMLLoader.load(getClass().getResource("menu.fxml"));
        Scene scene = new Scene(parent);
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
        window.setScene(scene);
        window.show();
    }}
